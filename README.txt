
taxonomy_similar will display a "similar tags" screen anytime content has
been created or updated with tags from a "Free tagging" vocabulary.

This module requires little configuration. Install it, enable it, give
the 'choose similar tags' permission to a user role and create or edit
content that has a "free tagging" vocabulary assigned to it.

The "similar tags" screen is entirely optional to the content creator - by
the time it appears, the new or edited content has already been saved to the
database. The content creator can edit his tags, or simply move on.

-----------------------------------------------------------------------------
 This module was made during the exploration and customization of Drupal
 by http://www.NHPR.org. In loving support of open source software,
 http://www.NHPR.org will continue to contribute patches they feel the
 community will benefit from. Questions about this module should be
 directed to morbus@disobey.com.
-----------------------------------------------------------------------------
